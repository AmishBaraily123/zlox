const std = @import("std");
const io = std.io;
const process = std.process;
const Allocator = std.mem.Allocator;

const zlox = @import("zlox");

const Chunk = zlox.Chunk;
const VM = zlox.VM;
const Compiler = zlox.Compiler;
const CompileError = zlox.CompileError;
const RuntimeError = zlox.RuntimeError;
const ExternalWriter = zlox.ExternalWriter;

const allocator = std.heap.wasm_allocator;

// sanity check
comptime {
    if (!@import("builtin").target.isWasm()) @compileError("wasm-lib.zig is being built for a non-WASM target");
}

// These functions are expected to be passed in as part of the WASM environment
extern "env" fn writeOut(ptr: [*]const u8, len: usize) void;
extern "env" fn writeErr(ptr: [*]const u8, len: usize) void;

pub fn writeOutSlice(bytes: []const u8) void {
    writeOut(bytes.ptr, bytes.len);
}

pub fn writeErrSlice(bytes: []const u8) void {
    writeErr(bytes.ptr, bytes.len);
}

pub export fn interpret(source: [*]const u8, len: usize) usize {
    const outWriter = ExternalWriter.init(writeOutSlice).writer();
    const errWriter = ExternalWriter.init(writeErrSlice).writer();

    var chunk = Chunk.init(allocator);
    defer chunk.deinit();

    var vm = VM.init(allocator, outWriter, errWriter, &chunk);
    defer vm.deinit();

    // translate the source code into bytecode
    var compiler = Compiler.init(&vm, outWriter, errWriter, source[0..len], &chunk);
    compiler.compile() catch return 65;

    // let the VM execute the bytecode
    vm.execute(null) catch return 70;

    return 0;
}

// allocation functions that operate on WASM memory
pub export fn alloc(len: usize) ?[*]u8 {
    return if (allocator.alloc(u8, len)) |slice| slice.ptr else |_| null;
}

pub export fn free(ptr: [*]const u8, len: usize) void {
    allocator.free(ptr[0..len]);
}
