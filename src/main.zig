const std = @import("std");
const builtin = @import("builtin");
const zlox = @import("zlox");

const CompileError = zlox.CompileError;
const RuntimeError = zlox.RuntimeError;

const Scanner = zlox.Scanner;
const Token = zlox.Token;
const TokenType = zlox.TokenType;
const Chunk = zlox.Chunk;
const VM = zlox.VM;
const Compiler = zlox.Compiler;

const stdout = std.io.getStdOut().writer();
const stdin = std.io.getStdIn().reader();
const stderr = std.io.getStdErr().writer();

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    defer {
        // in safe mode, we trigger a panic if there's a leak
        _ = gpa.deinit();
    }

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    switch (args.len) {
        1 => try repl(allocator),
        2 => try runFile(allocator, args[1]),
        else => printUsage(args[0]),
    }
}

fn repl(allocator: std.mem.Allocator) !void {
    var line = std.ArrayList(u8).init(allocator);
    defer line.deinit();

    while (true) {
        stdout.writeAll(">>> ") catch unreachable;
        stdin.streamUntilDelimiter(line.writer(), '\n', null) catch |err| {
            if (err == error.EndOfStream) {
                stdout.writeAll("\n") catch unreachable;
                break;
            }
            return err;
        };

        try interpret(allocator, line.items);

        line.clearRetainingCapacity();
    }
}

fn runFile(allocator: std.mem.Allocator, filename: []u8) !void {
    const source = try std.fs.cwd().readFileAlloc(allocator, filename, std.math.maxInt(usize));
    defer allocator.free(source);

    try interpret(allocator, source);
}

fn interpret(allocator: std.mem.Allocator, source: []const u8) !void {
    var chunk = Chunk.init(allocator);
    defer chunk.deinit();

    var vm = VM.init(allocator, stdout, stderr, &chunk);
    defer vm.deinit();

    // translate the source code into bytecode
    var compiler = Compiler.init(&vm, stdout, stderr, source, &chunk);
    try compiler.compile();

    // let the VM execute the bytecode
    try vm.execute(null);
}

fn printUsage(exe_name: [:0]u8) void {
    std.io.getStdErr().writer().print("Usage: {s} [file path]\n", .{exe_name}) catch unreachable;
    std.process.cleanExit();
}
