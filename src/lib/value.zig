const std = @import("std");
const object_lib = @import("object.zig");

const Object = object_lib.Object;
const ObjectType = object_lib.ObjectType;

/// Abstraction for the constants stored by the VM
pub const Value = union(enum) {
    boolean: bool,
    // TODO: add a nil type representing 0,
    // the compiler portion doesn't need the number variant to know that it's nil and the VM can
    // just output 0 on the stack
    number: f64,
    /// a heap allocated object
    object: *Object,

    pub fn format(value: Value, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        _ = fmt;
        switch (value) {
            .boolean => |boolean| try writer.print("{}", .{boolean}),
            .number => |number| try writer.print("{d}", .{number}),
            .object => |object| try writer.print("{}", .{object}),
        }
    }
    pub fn eq(self: Value, other: Value) bool {
        std.debug.assert(std.mem.eql(u8, @tagName(self), @tagName(other)));
        switch (self) {
            .boolean => return self.boolean == other.boolean,
            .number => {
                return self.number == other.number;
            },
            .object => {
                return self.object.compare(std.math.Order.eq, other.object);
            },
        }
    }
    pub fn greater(self: Value, other: Value) bool {
        std.debug.assert(std.mem.eql(u8, @tagName(self), @tagName(other)));
        switch (self) {
            // true > false
            .boolean => return self.boolean == true,
            .number => {
                return self.number > other.number;
            },
            .object => {
                return self.object.compare(std.math.Order.gt, other.object);
            },
        }
    }
    pub fn less(self: Value, other: Value) bool {
        std.debug.assert(std.mem.eql(u8, @tagName(self), @tagName(other)));
        switch (self) {
            // false < true
            .boolean => return self.boolean == false,
            .number => {
                return self.number < other.number;
            },
            .object => {
                return self.object.compare(std.math.Order.lt, other.object);
            },
        }
    }
    pub inline fn isNumber(self: Value) bool {
        return self == .number;
    }
    pub inline fn isNil(self: Value) bool {
        return self.isNumber() and self.number == 0;
    }
    pub inline fn asNumber(self: Value) f64 {
        std.debug.assert(self.isNumber());
        return self.number;
    }
    pub inline fn fromNumber(value: f64) Value {
        return Value{ .number = value };
    }
    pub inline fn isBoolean(self: Value) bool {
        return self == .boolean;
    }
    pub inline fn isBooleanValue(self: Value, value: bool) bool {
        return self.isBoolean() and self.boolean == value;
    }
    pub inline fn asBoolean(self: Value) bool {
        std.debug.assert(self.isBoolean());
        return self.boolean;
    }
    pub inline fn fromBoolean(value: bool) Value {
        return Value{ .boolean = value };
    }
    pub inline fn isObject(self: Value) bool {
        return self == .object;
    }
    pub inline fn isObjectType(self: Value, obj_type: ObjectType) bool {
        return self.isObject() and self.object.objectType == obj_type;
    }
    pub inline fn asObject(self: Value) *Object {
        std.debug.assert(self.isObject());
        return self.object;
    }
    pub inline fn fromObject(x: *Object) Value {
        return Value{ .object = x };
    }
};

/// A constant pool/array to hold large or variable-sized constants.
/// The instruction to load a constant will look up the value by index in this array.
pub const ValueArray = std.ArrayListAligned(Value, @alignOf(Value));
