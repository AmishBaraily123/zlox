const std = @import("std");

/// Classifies the type of instruction.
///
/// Used to determine which instruction is currently being executed.
pub const Opcode = enum(u8) {
    /// Produce a particular constant.
    ///
    /// This instruction should be followed by an operand which specifies which constant to load
    /// from a chunk's constant array.
    Constant,
    /// same as `Constant` but stores the operand as a 24-bit number
    Constant_Long,
    /// Return from current function.
    Return,
    /// Negates a value
    ///
    /// Takes one operand: the value to negate
    Negate,
    /// Unary Not
    Not,

    Equal,
    Greater,
    Less,
    // TODO: maybe create additional opcodes for !=, <= and >=

    Nil,
    True,
    False,

    Add,
    Subtract,
    Multiply,
    Divide,

    const Self = @This();

    /// Total number of variants in this enum.
    ///
    /// Integer value of variants start from 0 (this is an assumption I'm making), so the maximum
    /// value of the final variant will be one less than this total.
    pub const total_variants = @typeInfo(Self).Enum.fields.len;

    pub fn format(self: Self, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        _ = fmt;
        try switch (self) {
            .Return => writer.writeAll("RETURN"),
            .Constant => writer.writeAll("CONSTANT"),
            .Constant_Long => writer.writeAll("CONSTANT_LONG"),
            .Negate => writer.writeAll("NEGATE"),
            .Not => writer.writeAll("NOT"),
            .Add => writer.writeAll("ADD"),
            .Subtract => writer.writeAll("SUBTRACT"),
            .Multiply => writer.writeAll("MULTIPLY"),
            .Divide => writer.writeAll("DIVIDE"),
            .Nil => writer.writeAll("NIL"),
            .True => writer.writeAll("TRUE"),
            .False => writer.writeAll("FALSE"),
            .Equal => writer.writeAll("EQUAL"),
            .Greater => writer.writeAll("GREATER"),
            .Less => writer.writeAll("LESS"),
        };
    }
};
