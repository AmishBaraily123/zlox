const std = @import("std");

const Allocator = std.mem.Allocator;

const build_options = @import("build_options");

const chunk_lib = @import("chunk.zig");
const scanner_lib = @import("scanner.zig");
const error_lib = @import("error.zig");
const opcode_lib = @import("opcode.zig");
const value_lib = @import("value.zig");
const writer_lib = @import("writer.zig");
const vm_lib = @import("vm.zig");
const object_lib = @import("object.zig");

const Chunk = chunk_lib.Chunk;
const Token = scanner_lib.Token;
const TokenType = scanner_lib.TokenType;
const Scanner = scanner_lib.Scanner;
const CompileError = error_lib.CompileError;
const Opcode = opcode_lib.Opcode;
const Value = value_lib.Value;
const VMWriter = writer_lib.VMWriter;
const Object = object_lib.Object;
const VM = vm_lib.VM;
const String = object_lib.String;

// TODO: remove Parser, we can just directly embed it into Compiler
const Parser = struct {
    previous: Token = undefined,
    current: Token = undefined,
    panicMode: bool = false,
};

const Precedence = enum(u8) {
    None = 0,
    // =
    Assignment,
    // or
    Or,
    // and
    And,
    // == !=
    Equality,
    // < > <= >=
    Comparison,
    // ! -
    Term,
    // * /
    Factor,
    // ! -
    Unary,
    // . ()
    Call,
    Primary,
};

pub const Compiler = struct {
    allocator: Allocator,
    scanner: Scanner,
    compilingChunk: *Chunk,
    parser: Parser,
    vm: *VM,
    outWriter: VMWriter,
    errWriter: VMWriter,

    const ParseFn = ?*const (fn (self: *Compiler) CompileError!void);

    // defines for each TokenType:
    // - function to compile a prefix expression starting with a token of that type
    // - function to compile a infix expression whose left operand is followed by a token of that type
    // - the precedence of an infix expression that uses that token as an operator
    const ParseRule = struct {
        prefix: ParseFn,
        infix: ParseFn,
        precedence: Precedence,
    };

    // this is a table defining parse rule for each TokenType
    // so the number of elements is equal to the number of its fields
    var parse_rules: [TokenType.total_variants]ParseRule = undefined;

    pub fn init(vm: *VM, outWriter: VMWriter, errWriter: VMWriter, source: []const u8, chunk: *Chunk) Compiler {
        if (comptime parse_rules.len != 40) {
            @compileError("TokenType has fields != 40, make sure they're handled in Compiler");
        }

        fillParseRules();

        return Compiler{
            .vm = vm,
            .allocator = vm.allocator,
            .scanner = Scanner.init(source),
            .compilingChunk = chunk,
            .parser = Parser{},
            .outWriter = outWriter,
            .errWriter = errWriter,
        };
    }
    pub fn compile(self: *Compiler) CompileError!void {
        try self.advance();
        try self.expression();
        try self.consume(TokenType.Eof, "Expected end of expression.");
        if (comptime build_options.printBytecode) {
            if (!self.parser.panicMode) {
                self.compilingChunk.disassemble(self.outWriter, "code") catch unreachable;
            }
        }
        try self.emitReturn();
    }
    fn emitReturn(self: *Compiler) CompileError!void {
        try self.emitByte(Opcode.Return);
    }
    fn emitConstant(self: *Compiler, value: Value) CompileError!void {
        _ = self.compilingChunk.add_constant(value, self.parser.previous.line) catch |err| {
            var buf: [0x30]u8 = undefined;
            const msg = std.fmt.bufPrint(&buf, "Couldn't write constant to chunk ({})", .{err}) catch unreachable;
            self.errorAtPrevious(msg);
            return CompileError.ChunkWrite;
        };
    }
    fn emitByte(self: *Compiler, opcode: Opcode) CompileError!void {
        self.compilingChunk.write(@intFromEnum(opcode), self.parser.previous.line) catch |err| {
            var buf: [0x30]u8 = undefined;
            const msg = std.fmt.bufPrint(&buf, "Couldn't write byte to chunk ({})", .{err}) catch unreachable;
            self.errorAtPrevious(msg);
            return CompileError.ChunkWrite;
        };
    }
    fn advance(self: *Compiler) CompileError!void {
        self.parser.previous = self.parser.current;
        self.parser.current = self.scanner.scanToken();
        if (self.parser.current.type == TokenType.Invalid) {
            self.errorAtCurrent(self.parser.current.lexeme);
            return CompileError.InvalidCharacter;
        }
    }
    fn unary(self: *Compiler) CompileError!void {
        // leading `-` has been consumed previously
        const operatorType = self.parser.previous.type;

        // compile the operand
        try self.parsePrecedence(Precedence.Unary);

        // emit the operator instruction
        switch (operatorType) {
            TokenType.Minus => try self.emitByte(Opcode.Negate),
            TokenType.Bang => try self.emitByte(Opcode.Not),
            else => unreachable,
        }
    }

    fn literal(self: *Compiler) CompileError!void {
        switch (self.parser.previous.type) {
            .Nil => try self.emitByte(Opcode.Nil),
            .True => try self.emitByte(Opcode.True),
            .False => try self.emitByte(Opcode.False),
            else => unreachable,
        }
    }

    fn binary(self: *Compiler) CompileError!void {
        const operatorType = self.parser.previous.type;
        const rule = getRule(operatorType);
        try self.parsePrecedence(@enumFromInt(@intFromEnum(rule.precedence) + 1));
        switch (operatorType) {
            TokenType.Plus => try self.emitByte(Opcode.Add),
            TokenType.Minus => try self.emitByte(Opcode.Subtract),
            TokenType.Star => try self.emitByte(Opcode.Multiply),
            TokenType.Slash => try self.emitByte(Opcode.Divide),
            TokenType.EqualEqual => try self.emitByte(Opcode.Equal),
            TokenType.Greater => try self.emitByte(Opcode.Greater),
            TokenType.GreaterEqual => {
                try self.emitByte(Opcode.Less);
                try self.emitByte(Opcode.Not);
            },
            TokenType.Less => try self.emitByte(Opcode.Less),
            TokenType.LessEqual => {
                try self.emitByte(Opcode.Greater);
                try self.emitByte(Opcode.Not);
            },
            TokenType.BangEqual => {
                try self.emitByte(Opcode.Equal);
                try self.emitByte(Opcode.Not);
            },
            else => unreachable,
        }
    }

    fn getRule(ty: TokenType) *ParseRule {
        return &parse_rules[@intFromEnum(ty)];
    }

    fn parsePrecedence(self: *Compiler, precedence: Precedence) CompileError!void {
        try self.advance();
        if (getRule(self.parser.previous.type).prefix) |prefix_rule| {
            try prefix_rule(self);
        } else {
            self.errorAtPrevious("Invalid prefix expression");
            return CompileError.InvalidExpression;
        }

        while (@intFromEnum(precedence) <= @intFromEnum(getRule(self.parser.current.type).precedence)) {
            try self.advance();
            const infix_rule = getRule(self.parser.previous.type).infix;
            try infix_rule.?(self);
        }
    }
    fn grouping(self: *Compiler) CompileError!void {
        try self.expression();
        try self.consume(TokenType.RightParen, "Expected `)` after expression");
    }
    fn expression(self: *Compiler) CompileError!void {
        try self.parsePrecedence(Precedence.Assignment);
    }
    fn number(self: *Compiler) CompileError!void {
        const v = std.fmt.parseFloat(f64, self.parser.previous.lexeme) catch return CompileError.InvalidNumber;
        try self.emitConstant(Value{ .number = v });
    }
    fn string(self: *Compiler) CompileError!void {
        const str = self.parser.previous.lexeme[1..self.parser.previous.lexeme.len];
        var stringObject = String.initAlloc(self.vm, str);
        const stringValue = stringObject.obj.asValue();
        // TODO: inspect the layout of stringValue in memory with gdb
        // to verify our assumption of the memory layout of Object and its types
        try self.emitConstant(stringValue);
    }
    fn consume(self: *Compiler, token_type: TokenType, message: []const u8) CompileError!void {
        if (self.parser.current.type == token_type) {
            try self.advance();
            return;
        }
        self.errorAtCurrent(message);
        return CompileError.InvalidExpression;
    }
    fn errorAtCurrent(self: *Compiler, message: []const u8) void {
        self.errorAt(self.parser.current, message);
    }
    fn errorAtPrevious(self: *Compiler, message: []const u8) void {
        self.errorAt(self.parser.previous, message);
    }
    fn errorAt(self: *Compiler, token: Token, message: []const u8) void {
        // suppress other errors if panic mode is set
        if (self.parser.panicMode) return;
        self.parser.panicMode = true;

        self.errWriter.print("[line {d}] Compile Error", .{token.line}) catch unreachable;
        if (token.type == TokenType.Eof) {
            self.errWriter.writeAll(" at end") catch unreachable;
        } else {
            if (token.type != TokenType.Invalid) {
                self.errWriter.print(" at '{s}'", .{token.lexeme}) catch unreachable;
            }
        }
        self.errWriter.writeAll(": ") catch unreachable;
        self.errWriter.writeAll(message) catch unreachable;
        self.errWriter.writeByte('\n') catch unreachable;
    }
    fn fillParseRules() void {
        fillParseRule(TokenType.LeftParen, .{ .prefix = grouping, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.RightParen, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.LeftBrace, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.RightBrace, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Comma, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Dot, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Minus, .{ .prefix = unary, .infix = binary, .precedence = Precedence.Term });
        fillParseRule(TokenType.Plus, .{ .prefix = null, .infix = binary, .precedence = Precedence.Term });
        fillParseRule(TokenType.Semicolon, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Slash, .{ .prefix = null, .infix = binary, .precedence = Precedence.Factor });
        fillParseRule(TokenType.Star, .{ .prefix = null, .infix = binary, .precedence = Precedence.Factor });
        fillParseRule(TokenType.Bang, .{ .prefix = unary, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Nil, .{ .prefix = literal, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.True, .{ .prefix = literal, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.False, .{ .prefix = literal, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Equal, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.BangEqual, .{ .prefix = null, .infix = binary, .precedence = Precedence.Equality });
        fillParseRule(TokenType.EqualEqual, .{ .prefix = null, .infix = binary, .precedence = Precedence.Equality });
        fillParseRule(TokenType.Greater, .{ .prefix = null, .infix = binary, .precedence = Precedence.Comparison });
        fillParseRule(TokenType.GreaterEqual, .{ .prefix = null, .infix = binary, .precedence = Precedence.Comparison });
        fillParseRule(TokenType.Less, .{ .prefix = null, .infix = binary, .precedence = Precedence.Comparison });
        fillParseRule(TokenType.LessEqual, .{ .prefix = null, .infix = binary, .precedence = Precedence.Comparison });
        fillParseRule(TokenType.Identifier, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.String, .{ .prefix = string, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.And, .{ .prefix = number, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Number, .{ .prefix = number, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.And, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Class, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Else, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.For, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Fun, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.If, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Or, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Print, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Return, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Super, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.This, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Var, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.While, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Eof, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
        fillParseRule(TokenType.Invalid, .{ .prefix = null, .infix = null, .precedence = Precedence.None });
    }
    inline fn fillParseRule(token_type: TokenType, parse_rule: ParseRule) void {
        parse_rules[@intFromEnum(token_type)] = parse_rule;
    }
};
