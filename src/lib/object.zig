const std = @import("std");
const Allocator = std.mem.Allocator;

const value_lib = @import("value.zig");
const Value = value_lib.Value;

const vm_lib = @import("vm.zig");
const VM = vm_lib.VM;

pub const ObjectType = enum {
    String,
    pub fn format(value: ObjectType, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        _ = fmt;
        switch (value) {
            .String => try writer.writeAll("String"),
        }
    }
};

/// An abstract structure to represent a heap-allocated value in Wlox.
///
/// Encapsulates an ObjectType field which determines the type of object it represents.
///
/// Each ObjectType variant has its own corresponding struct defined acting as a specialisation of
/// the Object. The struct will hold an Object field itself. This way, we can cast a (valid) *Object
/// pointer to a pointer to its specialised struct.
///
/// The Object is also a node to a linked list of object. This way the VM can free every allocated
/// object by walking down this linked list.
///
/// In OOP sense, Object is the base class with its specialisations being the child class and we can
/// cast a pointer to a child class as a pointer to the base class and vice versa provided the
/// pointer reference valid memory.
pub const Object = struct {
    objectType: ObjectType,
    next: ?*Object,
    allocator: Allocator,

    /// You can't initialize the Object itself, you have to initialize the particular type of Object
    /// then you receive a pointer to it as an Object.
    pub fn init(vm: *VM, comptime T: type, objectType: ObjectType) *Object {
        var ptr = vm.allocator.create(T) catch @panic("OOM");
        ptr.obj.objectType = objectType;
        ptr.obj.allocator = vm.allocator;

        // add the newly created object to the head of the list tracked by VM
        ptr.obj.next = vm.objects;
        vm.objects = &ptr.obj;

        return &ptr.obj;
    }

    pub fn deinit(self: *Object) void {
        switch (self.objectType) {
            .String => {
                const str = self.asString();
                str.deinit();
                self.allocator.destroy(str);
            },
        }
    }

    pub fn compare(self: *Object, op: std.math.Order, other: *Object) bool {
        std.debug.assert(self.objectType == other.objectType);
        switch (self.objectType) {
            .String => {
                const aString = self.asString();
                const bString = other.asString();
                return aString.compare(op, bString);
            },
        }
    }

    pub inline fn isString(self: Object) bool {
        return self.objectType == .String;
    }

    /// given a pointer to Object as a base class (OOP), returns a pointer to String as the child
    /// class
    pub inline fn asString(self: *Object) *String {
        return @fieldParentPtr("obj", self);
    }

    pub fn asValue(self: *Object) Value {
        return Value.fromObject(self);
    }

    pub fn format(value: *Object, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        _ = fmt;
        switch (value.objectType) {
            .String => try writer.print("{}", .{value.asString()}),
        }
    }
};

pub const String = struct {
    obj: Object,
    str: []u8,
    hash: usize,
    vm: *VM,
    allocator: Allocator,

    const hasher = std.hash.Fnv1a_64;

    // Creates a String object but assumes the passed in string can be owned. The hash should be the
    // hash of the passed in string. This is to avoid redundant hash calculations. The ownership
    // will belong to the passed in VM's allocator.
    pub fn init(vm: *VM, str: []u8, hash: usize) *String {
        if (vm.strings.findString(str, hash)) |interned| {
            std.log.debug("returning interned string: \"{s}\"\n", .{str});
            vm.allocator.free(str);
            return interned;
        }
        const obj = Object.init(vm, String, .String);
        const out = obj.asString();
        out.hash = hash;
        out.str = str;
        out.vm = vm;
        out.allocator = vm.allocator;
        // since we know at this point a new string should be inserted `set()` should return true
        std.debug.assert(vm.strings.set(out, .{ .number = 0 }));
        return out;
    }

    /// Creates a String object and copies over the passed string.
    pub fn initAlloc(vm: *VM, str: []const u8) *String {
        // hash the given string using FNV-1a
        const hash = hasher.hash(str);
        if (vm.strings.findString(str, hash)) |interned| {
            // TODO: implement better logging mechanisms
            std.log.debug("returning interned string: \"{s}\"\n", .{str});
            return interned;
        }
        const ownedStr = vm.allocator.dupe(u8, str) catch @panic("OOM");
        return String.init(vm, ownedStr, hash);
    }

    pub fn deinit(self: String) void {
        self.allocator.free(self.str);
    }

    pub fn asObject(self: *String) *Object {
        return &self.obj;
    }

    pub fn concat(self: *const String, other: *const String) *String {
        // TODO: use std.mem.concat
        const self_len = self.str.len;
        const other_len = other.str.len;
        var buffer = self.allocator.alloc(u8, self_len + other_len) catch @panic("OOM");
        std.mem.copyForwards(u8, buffer[0..self_len], self.str);
        std.mem.copyForwards(u8, buffer[self_len..], other.str);
        const hash = hasher.hash(buffer);
        return String.init(self.vm, buffer, hash);
    }

    pub fn compare(self: *const String, op: std.math.Order, other: *const String) bool {
        return switch (op) {
            // since strings are interned, we don't need to do character by character comparison
            .eq => return self.str.ptr == other.str.ptr,
            else => std.mem.order(u8, self.str, other.str) == op,
        };
    }

    pub fn format(value: String, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        _ = fmt;
        try writer.print("{s}", .{value.str});
    }
};
