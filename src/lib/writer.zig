const std = @import("std");
const builtin = @import("builtin");
const File = std.fs.File;

pub const ExternalWriter = struct {
    pub const WriteFnType = *const fn (bytes: []const u8) void;
    pub const WriteError = error{};

    writeFn: WriteFnType,

    pub fn init(writeFn: WriteFnType) ExternalWriter {
        return ExternalWriter{ .writeFn = writeFn };
    }

    pub fn write(self: ExternalWriter, bytes: []const u8) WriteError!usize {
        self.writeFn(bytes);
        return bytes.len;
    }

    pub const Writer = std.io.Writer(ExternalWriter, WriteError, write);
    pub fn writer(self: ExternalWriter) Writer {
        return .{ .context = self };
    }
};

const is_wasm_freestanding = builtin.target.isWasm() and builtin.target.os.tag == .freestanding;

pub const VMWriter = if (is_wasm_freestanding) ExternalWriter.Writer else File.Writer;
