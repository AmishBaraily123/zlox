const std = @import("std");
const builtin = @import("builtin");
const build_options = @import("build_options");

const Allocator = std.mem.Allocator;

const chunk_lib = @import("chunk.zig");
const opcode_lib = @import("opcode.zig");
const error_lib = @import("error.zig");
const value_lib = @import("value.zig");
const object_lib = @import("object.zig");
const writer_lib = @import("writer.zig");
const table_lib = @import("table.zig");

const Chunk = chunk_lib.Chunk;
const Opcode = opcode_lib.Opcode;
const Value = value_lib.Value;
const ValueArray = value_lib.ValueArray;
const VMWriter = writer_lib.VMWriter;
const Object = object_lib.Object;
const Table = table_lib.Table;

const RuntimeError = error_lib.RuntimeError;

const ValueTupleType = struct { Value, Value };

const STACK_SIZE: comptime_int = 0x100;

/// The virtual machine that executes instructions contained in a `Chunk`
pub const VM = struct {
    // a function that handles a particular Opcode variant
    const OpcodeHandler = *const (fn (self: *VM) RuntimeError!void);

    allocator: Allocator,
    chunk: *Chunk,
    ip: usize,
    handlers: []OpcodeHandler,
    strings: Table,
    objects: ?*Object,
    outWriter: VMWriter,
    errWriter: VMWriter,

    /// A buffer for the VM's stack.
    stack: ValueArray,

    const Self = @This();
    /// Creates a new VM instance ready to execute instructions from the given `Chunk`.
    ///
    /// The caller is responsible for memory holding the given `chunk`.
    ///
    /// The given chunk pointer can be changed later from `execute()` thus supporting execution of
    /// multiple chunks by a single VM instance.
    ///
    /// It is assumed that the given `chunk` pointer is valid.
    pub fn init(allocator: Allocator, outWriter: VMWriter, errWriter: VMWriter, chunk: *Chunk) Self {
        return Self{
            .allocator = allocator,
            .chunk = chunk,
            .ip = 0,
            .handlers = getHandlers(allocator),
            .stack = ValueArray.init(allocator),
            .objects = null,
            .strings = Table.init(allocator),
            .outWriter = outWriter,
            .errWriter = errWriter,
        };
    }

    pub fn deinit(self: *Self) void {
        self.strings.deinit();
        self.stack.deinit();
        self.freeObjects();
        self.allocator.free(self.handlers);
    }

    fn freeObjects(self: Self) void {
        var object = self.objects;
        while (object) |obj| {
            const next = obj.next;
            obj.deinit();
            object = next;
        }
    }

    /// Execute the given `Chunk` during initialization or a new one.
    pub fn execute(self: *Self, new_chunk: ?*Chunk) !void {
        if (new_chunk) |chunk| {
            self.chunk = chunk;
        }
        self.stack.clearRetainingCapacity();
        try self.run();
    }

    // runs the instructions
    fn run(self: *Self) RuntimeError!void {
        if (comptime build_options.verbose) {
            self.outWriter.writeAll("executing instructions\n") catch unreachable;
        }
        while (self.ip < self.chunk.code_count) {
            const byte = self.chunk.code[self.ip];
            switch (byte) {
                0...Opcode.total_variants => {
                    if (comptime build_options.displayStack) {
                        self.outWriter.writeAll("stack ->\t\t") catch unreachable;
                        for (self.stack.items) |*slot| {
                            self.outWriter.print("[ {s} ]", .{slot.*}) catch @panic("failed to print stack");
                        }
                        self.outWriter.writeAll("\n") catch unreachable;
                        _ = Chunk.disassemble_at(self.chunk, self.outWriter, self.ip) catch @panic("failed to disassemble instruction");
                    }
                    try self.handlers[byte](self);
                },
                else => {
                    self.reportError("Invalid opcode encountered");
                    return RuntimeError.InvalidInstruction;
                },
            }
        }
    }

    fn reportError(self: *VM, msg: []const u8) void {
        // the interpreter advances past each instruction before executing it so the failed
        // instruction is the previous one.
        const line = self.chunk.get_line(self.ip - 1);
        self.errWriter.print("[line {d} in source] Runtime error: {s}\n", .{ line, msg }) catch unreachable;
    }

    fn getHandlers(allocator: Allocator) []OpcodeHandler {
        const total = Opcode.total_variants;
        var handlers = allocator.alloc(OpcodeHandler, total) catch @panic("OOM");
        for (0..total) |i| {
            const opcode: Opcode = @enumFromInt(i);
            switch (opcode) {
                .Return => {
                    handlers[i] = VM.handleReturn;
                },
                .Constant => {
                    handlers[i] = VM.handleConstant;
                },
                .Constant_Long => {
                    handlers[i] = VM.handleConstantLong;
                },
                .Equal => {
                    handlers[i] = VM.handleEqual;
                },
                .Greater => {
                    handlers[i] = VM.handleGreater;
                },
                .Less => {
                    handlers[i] = VM.handleLess;
                },
                .Nil, .True, .False => {
                    handlers[i] = VM.handleLiteral;
                },
                .Negate => {
                    handlers[i] = VM.handleNegate;
                },
                .Not => {
                    handlers[i] = VM.handleNot;
                },
                .Add => {
                    handlers[i] = VM.handleAdd;
                },
                .Subtract => {
                    handlers[i] = VM.handleSubtract;
                },
                .Multiply => {
                    handlers[i] = VM.handleMultiply;
                },
                .Divide => {
                    handlers[i] = VM.handleDivide;
                },
            }
        }
        return handlers;
    }

    pub fn handleLiteral(self: *VM) RuntimeError!void {
        // Safety: @enumFromInt should succeed since we only arrived here (from run()) after we knew the byte was
        // a valid opcode
        const opcode: Opcode = @enumFromInt(self.chunk.code[self.ip]);
        switch (opcode) {
            .Nil => self.stack.append(Value{ .number = 0 }) catch @panic("OOM"),
            .True => self.stack.append(Value{ .boolean = true }) catch @panic("OOM"),
            .False => self.stack.append(Value{ .boolean = false }) catch @panic("OOM"),
            else => unreachable,
        }
        self.ip += 1;
    }

    pub fn handleReturn(self: *VM) RuntimeError!void {
        const value = try self.stackPop();
        if (comptime build_options.verbose) {
            self.outWriter.print("returned value: {}\n", .{value}) catch unreachable;
        }
        self.ip += 1;
    }

    pub fn handleConstant(self: *VM) RuntimeError!void {
        const idx: usize = @as(u8, self.chunk.code[self.ip + 1]);

        const value = self.chunk.constants.items[idx];

        self.stack.append(value) catch return RuntimeError.StackOverflow;

        self.ip += 2;
    }

    pub fn handleConstantLong(self: *VM) RuntimeError!void {
        const offset = self.ip;
        const bytes = [_]u8{ self.chunk.code[offset + 1], self.chunk.code[offset + 2], self.chunk.code[offset + 3] };
        // get the 24-bit operand
        const operand = @as(u24, @bitCast(bytes));
        // handle endianness
        const idx = if (comptime builtin.cpu.arch.endian() == .little) @byteSwap(operand) else operand;
        const value = self.chunk.constants.items[idx];

        self.stack.append(value) catch return RuntimeError.StackOverflow;

        self.ip += 4;
    }

    pub fn handleNegate(self: *VM) RuntimeError!void {
        const original = try self.stackPop();
        const negated = switch (original) {
            .number => Value{ .number = -original.number },
            else => {
                self.reportError("Operand must be a number");
                return RuntimeError.InvalidOperandType;
            },
        };
        self.stack.append(negated) catch unreachable;

        self.ip += 1;
    }

    pub fn handleNot(self: *VM) RuntimeError!void {
        const original = try self.stackPop();

        self.stack.append(Value{ .boolean = isFalsey(original) }) catch unreachable;

        self.ip += 1;
    }
    inline fn isFalsey(value: Value) bool {
        return switch (value) {
            .boolean => !value.boolean,
            // !nil == true
            .number => value.number == 0,
            .object => false,
        };
    }

    pub fn handleAdd(self: *VM) RuntimeError!void {
        try self.handleBinaryOp(Opcode.Add);
        self.ip += 1;
    }

    pub fn handleSubtract(self: *VM) RuntimeError!void {
        try self.handleBinaryOp(Opcode.Subtract);
        self.ip += 1;
    }

    pub fn handleMultiply(self: *VM) RuntimeError!void {
        try self.handleBinaryOp(Opcode.Multiply);
        self.ip += 1;
    }

    pub fn handleDivide(self: *VM) RuntimeError!void {
        try self.handleBinaryOp(Opcode.Divide);
        self.ip += 1;
    }

    pub fn handleEqual(self: *VM) RuntimeError!void {
        try self.handleBinaryOp(Opcode.Equal);
        self.ip += 1;
    }

    pub fn handleGreater(self: *VM) RuntimeError!void {
        try self.handleBinaryOp(Opcode.Greater);
        self.ip += 1;
    }

    pub fn handleLess(self: *VM) RuntimeError!void {
        try self.handleBinaryOp(Opcode.Less);
        self.ip += 1;
    }
    fn handleBinaryOp(self: *VM, opcode: Opcode) RuntimeError!void {
        const popped: ValueTupleType = try self.stackPopTwo();
        const right: Value = popped.@"0";
        const left: Value = popped.@"1";
        if (!std.mem.eql(u8, @tagName(left), @tagName(right))) {
            const buffer = std.fmt.allocPrint(self.allocator, "Operation `{}` not supported for operands of type `{}` and `{}", .{ opcode, left, right }) catch @panic("OOM");
            defer self.allocator.free(buffer);
            self.reportError(buffer);
            return RuntimeError.InvalidOperandType;
        }
        const result_value: Value = switch (opcode) {
            .Equal => .{ .boolean = left.eq(right) },
            .Greater => .{ .boolean = left.greater(right) },
            .Less => .{ .boolean = left.less(right) },
            .Subtract => blk: {
                // we have checked that left and right are of same tag type so if left is number,
                // right must also be number
                if (left.isNumber()) break :blk .{ .number = left.number - right.number };
                self.reportError("Both operands must be numbers for subtraction");
                return RuntimeError.InvalidOperandType;
            },
            .Multiply => blk: {
                if (left.isNumber()) break :blk .{ .number = left.number * right.number };
                self.reportError("Both operands must be numbers for multiplication");
                return RuntimeError.InvalidOperandType;
            },
            .Divide => blk: {
                if (left.isNumber()) {
                    if (right.number == 0) {
                        self.reportError("Attempt to divide by zero");
                        return RuntimeError.DivideByZero;
                    }
                    break :blk .{ .number = left.number / right.number };
                }
                self.reportError("Both operands must be numbers for division");
                return RuntimeError.InvalidOperandType;
            },
            .Add => blk: {
                if (left.isObjectType(.String) and right.isObjectType(.String)) {
                    const leftStr = left.asObject().asString();
                    const rightStr = right.asObject().asString();
                    var concatStr = leftStr.concat(rightStr);
                    break :blk concatStr.asObject().asValue();
                } else if (left.isNumber()) {
                    break :blk .{ .number = left.asNumber() + right.asNumber() };
                } else {
                    self.reportError("Operands must be strings or numbers");
                    return RuntimeError.InvalidOperandType;
                }
            },
            else => unreachable,
        };
        self.stack.append(result_value) catch unreachable;
    }

    fn stackPopTwo(self: *VM) RuntimeError!ValueTupleType {
        const right = try self.stackPop();
        const left = try self.stackPop();
        return .{ right, left };
    }

    fn stackPop(self: *VM) RuntimeError!Value {
        return self.stack.popOrNull() orelse {
            self.reportError("Not enough operands present on the stack for given instruction");
            return RuntimeError.StackUnderflow;
        };
    }
};

// ========================= TESTS =================================

test "negate" {
    const allocator = std.testing.allocator;

    var first_chunk = Chunk.init(allocator);
    defer first_chunk.deinit();

    _ = try first_chunk.add_constant(.{ .long = 3.1415 }, 124);
    try first_chunk.write(@intFromEnum(Opcode.Negate), 124);

    var vm = VM.init(allocator, &first_chunk);
    defer vm.deinit();

    try vm.execute(null);

    try std.testing.expect(1 == vm.stack.items.len);
    try std.testing.expect(-3.1415 == vm.stack.items[0].number);
}

test "invalid operands" {
    const allocator = std.testing.allocator;

    var chunk = Chunk.init(allocator);

    var vm = VM.init(allocator, &chunk);
    defer vm.deinit();

    var lineno: usize = 0x100;
    {
        defer chunk.deinit();

        _ = try chunk.add_constant(.{ .boolean = true }, lineno);
        lineno += 1;

        _ = try chunk.add_constant(.{ .short = 10 }, lineno);
        lineno += 1;

        try chunk.write(@intFromEnum(Opcode.Add), lineno);

        try std.testing.expectError(RuntimeError.InvalidOperandType, vm.execute(null));
    }

    chunk = Chunk.init(allocator);
    {
        defer chunk.deinit();

        _ = try chunk.add_constant(.{ .boolean = true }, lineno);
        lineno += 1;

        _ = try chunk.add_constant(.{ .boolean = false }, lineno);
        lineno += 1;

        try chunk.write(@intFromEnum(Opcode.Multiply), lineno);

        try std.testing.expectError(RuntimeError.InvalidOperandType, vm.execute(&chunk));
    }

    chunk = Chunk.init(allocator);
    {
        defer chunk.deinit();

        _ = try chunk.add_constant(.{ .short = 3.14 }, lineno);
        lineno += 1;

        _ = try chunk.add_constant(.{ .long = 10 }, lineno);
        lineno += 1;

        try chunk.write(@intFromEnum(Opcode.Divide), lineno);

        try std.testing.expectError(RuntimeError.InvalidOperandType, vm.execute(&chunk));
    }
}

test "arithmetic" {
    const allocator = std.testing.allocator;
    var chunk = Chunk.init(allocator);
    defer chunk.deinit();

    var vm = VM.init(allocator, &chunk);
    defer vm.deinit();

    const expected = 10.12 - ((1.2 + 3.4) / 5.6);

    var lineno: usize = 0x100;

    _ = try chunk.add_constant(.{ .short = 10.12 }, lineno);
    lineno += 1;

    _ = try chunk.add_constant(.{ .short = 1.2 }, lineno);
    lineno += 1;

    _ = try chunk.add_constant(.{ .short = 3.4 }, lineno);
    lineno += 1;

    try chunk.write(@intFromEnum(Opcode.Add), lineno);
    lineno += 1;

    _ = try chunk.add_constant(.{ .short = 5.6 }, lineno);
    lineno += 1;
    try chunk.write(@intFromEnum(Opcode.Divide), lineno);
    lineno += 1;

    try chunk.write(@intFromEnum(Opcode.Subtract), lineno);

    try vm.execute(null);

    try std.testing.expect(1 == vm.stack.items.len);
    try std.testing.expect(expected == vm.stack.items[0].number);
}
