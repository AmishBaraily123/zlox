const std = @import("std");

const object_lib = @import("object.zig");
const value_lib = @import("value.zig");

const String = object_lib.String;
const Value = value_lib.Value;
const Allocator = std.mem.Allocator;

pub const Entry = struct {
    key: ?*String = null,
    value: Value = .{ .number = 0 },

    /// Tombstone is an entry with empty key and true value used to denote deleted entries in the
    /// hash table
    pub fn initTombstone() Entry {
        return Entry{
            .key = null,
            .value = .{ .boolean = true },
        };
    }
    pub inline fn isTombstone(self: Entry) bool {
        return if (self.key != null) false else self.value.isBooleanValue(true);
    }
};

// TODO: make this hash table module generic for other object types
/// Hash Table as an array of `Entry` elements
pub const Table = struct {
    entries: []Entry,
    /// entries.len gives the size of the allocated array whereas this gives the number of elements
    /// in the array that are actually useful i.e. number of key/value pairs currently stored. this
    /// also includes the number of tombstones.
    count: usize,
    allocator: Allocator,

    pub fn init(allocator: Allocator) Table {
        return Table{
            .count = 0,
            .allocator = allocator,
            .entries = &[_]Entry{},
        };
    }

    pub fn deinit(self: *Table) void {
        self.allocator.free(self.entries);
        self.count = 0;
    }

    /// Adds the given key/value pair to the hash table. If the bucket for this pair isn't empty,
    /// then it will overwrite the existing entry. Returns true if a new pair was inserted.
    pub fn set(self: *Table, key: *String, value: Value) bool {
        const current_capacity = self.entries.len;
        // load factor = 3/4 = 0.75
        // doing it this way since we can't do current_capacity * 0.75 as the two are incompatible
        // types
        if (4 * (self.count + 1) > 3 * current_capacity) {
            self.increaseCapacity();
        }
        const entry = findEntry(self.entries, key);
        const isNewKey = entry.key == null;
        // increment the count only if the new entry goes in an entirely empty bucket
        if (isNewKey and entry.value.isNil()) self.count += 1;

        entry.key = key;
        entry.value = value;

        return isNewKey;
    }

    /// Copy all of the entries from `from` to `self` modifying it in-place.
    pub fn copy(self: *Table, from: Table) !void {
        for (from.entries) |entry| {
            if (entry.key) |key| {
                _ = try self.set(key, entry.value);
            }
        }
    }

    /// Delete the key/value pair given by `key`.
    ///
    /// Returns false if an entry with the key wasn't found.
    pub fn delete(self: *Table, key: *const String) bool {
        if (self.count == 0) return false;
        const entry = findEntry(self.entries, key);
        if (entry.key) {
            // if found, replace with a tombstone
            entry.* = Entry.initTombstone();
        }
        return false;
    }

    /// Given a key, returns a pointer to the corresponding value if found or null.
    pub fn get(self: *Table, key: *const String) ?Value {
        if (self.count == 0) return null;
        const entry = findEntry(self.entries, key);
        if (entry.key) return entry.value;
        return null;
    }

    fn increaseCapacity(self: *Table) void {
        const new_capacity = grow_capacity(self.entries.len);
        const new_entries = self.allocator.alloc(Entry, new_capacity) catch @panic("OOM");
        for (new_entries) |*entry| {
            entry.key = null;
            entry.value = .{ .number = 0 };
        }
        // since we choose the bucket for an entry based on the internal array's size, so we
        // need to rebuild the hash table every time the size changes
        self.rebuild(new_entries);
        // the first entry is a garbage one, so we don't need to free it
        if (self.entries.len > 1) self.allocator.free(self.entries);
        self.entries = new_entries;
    }

    // reassign key/value pairs from old buckets to new locations. use after the internal array has been resized.
    fn rebuild(self: *Table, new_entries: []Entry) void {
        // reset the count discarding all of the tombstone entries
        self.count = 0;
        for (self.entries) |entry| {
            // take the old entry's key
            if (entry.key) |key| {
                // and try to find a suitable bucket for it in the new array
                const dest = findEntry(new_entries, key);
                // assign the values
                dest.key = key;
                dest.value = entry.value;
                self.count += 1;
            }
        }
    }

    /// Find the appropriate bucket for the given key. The bucket may or may not contain an existing
    /// key. In particular, if `entry.key == null`, then an empty bucket is returned.
    pub fn findString(self: *Table, str: []const u8, hash: usize) ?*String {
        if (self.count == 0) return null;

        var index: usize = hash % self.entries.len;
        while (true) {
            const entry = &self.entries[index];
            // stop if we find an empty non-tombstone entry
            if (entry.key == null and entry.value.isNil()) {
                break;
            } else {
                const key = entry.key.?;
                const eql = std.mem.eql;
                // found the key
                if (key.str.len == str.len and key.hash == hash and eql(u8, key.str, str)) return key;
            }
            index = (index + 1) % self.entries.len;
        }
        return null;
    }

    /// Find the appropriate bucket for the given key. The bucket may or may not contain an existing
    /// key. In particular, if `entry.key == null`, then an empty bucket is returned.
    fn findEntry(entries: []Entry, key: *const String) *Entry {
        var index: usize = key.hash % entries.len;
        var maybeTombstone: ?*Entry = null;
        while (true) {
            const entry: *Entry = &entries[index];
            if (entry.key != null and entry.key.? == key) {
                return entry;
            } else {
                // if we pass a tombstone entry, we note it. next time if we find an empty bucket
                // but we had recorded a tombstone entry before then we just return the tombstone
                // entry. this way we can treat the tombstones as empty buckets and reuse them.
                if (entry.isTombstone() and maybeTombstone == null) maybeTombstone = entry else return maybeTombstone orelse entry;
            }
            // using linear probing
            index = (index + 1) % entries.len;
        }
        unreachable;
    }

    inline fn grow_capacity(old: usize) usize {
        return if (old < 16) 16 else 2 * old;
    }
};
