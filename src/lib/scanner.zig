const std = @import("std");

pub const Token = struct {
    type: TokenType,
    lexeme: []const u8,
    line: usize,
};

pub const TokenType = enum {
    // Single-character tokens.
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,

    // One or two character tokens.
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    // Literals.
    Identifier,
    String,
    Number,

    // Keywords.
    And,
    Class,
    Else,
    False,
    For,
    Fun,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,
    Eof,

    Invalid,

    pub const total_variants = @typeInfo(TokenType).Enum.fields.len;

    pub fn format(value: TokenType, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = options;
        _ = fmt;
        switch (value) {
            .LeftParen => try writer.writeAll("LeftParen"),
            .RightParen => try writer.writeAll("RightParen"),
            .LeftBrace => try writer.writeAll("LeftBrace"),
            .RightBrace => try writer.writeAll("RightBrace"),
            .Comma => try writer.writeAll("Comma"),
            .Dot => try writer.writeAll("Dot"),
            .Minus => try writer.writeAll("Minus"),
            .Plus => try writer.writeAll("Plus"),
            .Semicolon => try writer.writeAll("Semicolon"),
            .Slash => try writer.writeAll("Slash"),
            .Star => try writer.writeAll("Star"),
            .Bang => try writer.writeAll("Bang"),
            .BangEqual => try writer.writeAll("BangEqual"),
            .Equal => try writer.writeAll("Equal"),
            .EqualEqual => try writer.writeAll("EqualEqual"),
            .Greater => try writer.writeAll("Greater"),
            .GreaterEqual => try writer.writeAll("GreaterEqual"),
            .Less => try writer.writeAll("Less"),
            .LessEqual => try writer.writeAll("LessEqual"),
            .Identifier => try writer.writeAll("Identifier"),
            .String => try writer.writeAll("String"),
            .Number => try writer.writeAll("Number"),
            .And => try writer.writeAll("And"),
            .Class => try writer.writeAll("Class"),
            .Else => try writer.writeAll("Else"),
            .False => try writer.writeAll("False"),
            .For => try writer.writeAll("For"),
            .Fun => try writer.writeAll("Fun"),
            .If => try writer.writeAll("If"),
            .Nil => try writer.writeAll("Nil"),
            .Or => try writer.writeAll("Or"),
            .Print => try writer.writeAll("Print"),
            .Return => try writer.writeAll("Return"),
            .Super => try writer.writeAll("Super"),
            .This => try writer.writeAll("This"),
            .True => try writer.writeAll("True"),
            .Var => try writer.writeAll("Var"),
            .While => try writer.writeAll("While"),
            .Eof => try writer.writeAll("Eof"),
            .Invalid => try writer.writeAll("Invalid"),
        }
    }
};

pub const Scanner = struct {
    source: []const u8,
    start: usize,
    current: usize,
    line: usize,

    pub fn init(source: []const u8) Scanner {
        return Scanner{
            .source = source,
            .start = 0,
            .current = 0,
            .line = 1,
        };
    }

    pub fn scanToken(self: *Scanner) Token {
        self.skip();
        self.start = self.current;

        if (self.isAtEnd()) return self.makeToken(TokenType.Eof);

        const c = self.advance();

        switch (c) {
            '(' => return self.makeToken(TokenType.LeftParen),
            ')' => return self.makeToken(TokenType.RightParen),
            '{' => return self.makeToken(TokenType.LeftBrace),
            '}' => return self.makeToken(TokenType.RightBrace),
            ',' => return self.makeToken(TokenType.Comma),
            '-' => return self.makeToken(TokenType.Minus),
            '+' => return self.makeToken(TokenType.Plus),
            ';' => return self.makeToken(TokenType.Semicolon),
            '/' => return self.makeToken(TokenType.Slash),
            '*' => return self.makeToken(TokenType.Star),
            '!' => {
                return self.makeToken(if (self.match('=')) TokenType.BangEqual else TokenType.Bang);
            },
            '=' => {
                return self.makeToken(if (self.match('=')) TokenType.EqualEqual else TokenType.Equal);
            },
            '>' => {
                return self.makeToken(if (self.match('=')) TokenType.GreaterEqual else TokenType.Greater);
            },
            '<' => {
                return self.makeToken(if (self.match('=')) TokenType.LessEqual else TokenType.Less);
            },
            '"' => {
                return self.match_string();
            },
            '0'...'9' => {
                return self.match_number();
            },
            else => {
                if (std.ascii.isAlphabetic(c)) {
                    return self.match_identifier();
                }
            },
        }

        return self.invalidToken("unexpected character");
    }

    fn makeToken(self: *Scanner, ty: TokenType) Token {
        return Token{
            .type = ty,
            .lexeme = self.source[self.start..self.current],
            .line = self.line,
        };
    }

    fn invalidToken(self: *Scanner, msg: []const u8) Token {
        return Token{
            .type = TokenType.Invalid,
            .lexeme = msg,
            .line = self.line,
        };
    }

    fn isAtEnd(self: *Scanner) bool {
        return self.current >= self.source.len;
    }

    fn advance(self: *Scanner) u8 {
        if (comptime std.debug.runtime_safety) {
            if (self.isAtEnd()) @panic("would overflow if advanced");
        }
        const c = self.source[self.current];
        self.current += 1;
        return c;
    }

    fn match(self: *Scanner, expected: u8) bool {
        if (self.isAtEnd() or (self.source[self.current] != expected)) return false;
        self.current += 1;
        return true;
    }

    // skip whitespace and comments
    fn skip(self: *Scanner) void {
        infinite_loop: while (true) {
            const current_char = self.peek() orelse break;
            switch (current_char) {
                ' ', '\r', '\t', '\n' => {
                    if (current_char == '\n') {
                        self.line += 1;
                    }
                    _ = self.advance();
                },
                '/' => {
                    if ((self.peekNext() orelse break :infinite_loop) == '/') {
                        // we encountered a comment, so keep advancing until we hit a new line
                        while ((self.peek() orelse break :infinite_loop) != '\n') {
                            _ = self.advance();
                        }
                    } else {
                        break :infinite_loop;
                    }
                },
                else => break :infinite_loop,
            }
        }
    }

    inline fn peek(self: *Scanner) ?u8 {
        if (self.isAtEnd()) return null;
        return self.source[self.current];
    }

    inline fn peekNext(self: *Scanner) ?u8 {
        if (self.current >= self.source.len - 1) return null;
        return self.source[self.current + 1];
    }

    fn match_string(self: *Scanner) Token {
        // advance until the string is terminated
        while (self.peek()) |c| {
            if (c == '"') {
                break;
            } else {
                if (c == '\n') {
                    self.line += 1;
                }
                _ = self.advance();
            }
        }

        // we could only reach here if the string was terminated or we reached EOF,
        // determine if it was the latter part
        if (self.isAtEnd()) return self.invalidToken("unterminated string");

        // the lexeme is all that we've gathered up until now,
        // excluding the closing quote
        const token = self.makeToken(TokenType.String);
        _ = self.advance();
        return token;
    }

    fn match_number(self: *Scanner) Token {
        // consume the digits
        while (true) {
            const c = self.peek() orelse break;
            if (!std.ascii.isDigit(c)) {
                break;
            }
            _ = self.advance();
        }

        // check if there's a fractional part
        if (self.peek()) |c| {
            if (c == '.') {
                var next_digit = self.peekNext() orelse @panic(
                    \\encountered a number which didn't follow with a digit after the decimal point
                    \\
                );
                // consume the digits of the fractional part...
                while (std.ascii.isDigit(next_digit)) {
                    _ = self.advance();
                    // ... and then some more
                    next_digit = self.peek() orelse break;
                }
            }
        }

        return self.makeToken(TokenType.Number);
    }

    fn match_identifier(self: *Scanner) Token {
        while (self.peek()) |c| {
            if (!std.ascii.isAlphanumeric(c)) {
                break;
            }
            _ = self.advance();
        }

        return self.makeToken(self.identifierType());
    }

    fn identifierType(self: *Scanner) TokenType {
        // we implement a trie to match keywords
        switch (self.source[self.start]) {
            'a' => return self.check_keyword(1, "nd", TokenType.And),
            'c' => return self.check_keyword(1, "class", TokenType.Class),
            'e' => return self.check_keyword(1, "lse", TokenType.Else),
            'f' => {
                if (self.current - self.start > 1) {
                    switch (self.source[self.start + 1]) {
                        'a' => return self.check_keyword(2, "lse", TokenType.False),
                        'o' => return self.check_keyword(2, "r", TokenType.For),
                        'u' => return self.check_keyword(2, "n", TokenType.Fun),
                        else => return TokenType.Identifier,
                    }
                }
            },
            'i' => return self.check_keyword(1, "f", TokenType.If),
            'n' => return self.check_keyword(1, "il", TokenType.Nil),
            'o' => return self.check_keyword(1, "r", TokenType.Or),
            'p' => return self.check_keyword(1, "rint", TokenType.Print),
            'r' => return self.check_keyword(1, "eturn", TokenType.Return),
            's' => return self.check_keyword(1, "uper", TokenType.Super),
            't' => {
                if (self.current - self.start > 1) {
                    switch (self.source[self.start + 1]) {
                        'h' => return self.check_keyword(2, "is", TokenType.This),
                        'r' => return self.check_keyword(2, "ue", TokenType.True),
                        else => return TokenType.Identifier,
                    }
                }
            },
            'v' => return self.check_keyword(1, "ar", TokenType.Var),
            'w' => return self.check_keyword(1, "hile", TokenType.While),
            else => return TokenType.Identifier,
        }
        unreachable;
    }

    fn check_keyword(self: *Scanner, start: usize, rest: []const u8, ty: TokenType) TokenType {
        const eql = std.mem.eql;
        const keyword_len = start + rest.len;
        // the lexeme we have captured must also have the same length as the keyword we're
        // checking against
        if (self.current - self.start == keyword_len and
            eql(u8, self.source[self.start + start .. self.current], rest))
        {
            return ty;
        }

        return TokenType.Identifier;
    }
};
