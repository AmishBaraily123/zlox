const std = @import("std");
const builtin = @import("builtin");
const value_lib = @import("value.zig");
const opcode_lib = @import("opcode.zig");
const build_options = @import("build_options");
const writer_lib = @import("writer.zig");

const Allocator = std.mem.Allocator;
const Value = value_lib.Value;
const ValueArray = value_lib.ValueArray;
const Opcode = opcode_lib.Opcode;
const VMWriter = writer_lib.VMWriter;

/// For storing line-information using run-length encoding.
///
/// Each `Line` will store the line number from the source code as well as the offset within the
/// `code` of `Chunk` where the first instruction of that line begins.
pub const Line = struct {
    /// the line number from the source code corresponding to one or more instructions
    lineno: usize,
    /// the offset within the `code` array in `Chunk` corresponding to where the first instruction of that
    /// line begins
    offset: usize,
};

/// A dynamic array to hold a series of instructions.
///
// Note: I'm implementing the dynamic array myself because I wanted to work with less abstraction;
// the sole motivating factor behind it being the learning experience.
pub const Chunk = struct {
    allocator: Allocator,
    /// stores all the data and instruction of the chunk
    code: []u8,
    /// the number of valid bytes contained within `code`
    code_count: usize,
    /// the constant pool
    constants: ValueArray,
    /// Array of line numbers paralleling `code`.
    ///
    /// This is used for error reporting.
    lines: []Line,
    /// number of valid `Line`s in `lines`
    lines_count: usize,

    const Self = @This();

    pub fn init(allocator: Allocator) Self {
        return Self{
            .allocator = allocator,
            .code = &[_]u8{},
            .code_count = 0,
            .lines = &[_]Line{},
            .lines_count = 0,
            .constants = ValueArray.init(allocator),
        };
    }

    pub fn deinit(self: *Self) void {
        self.code_count = 0;
        self.allocator.free(self.code);
        self.allocator.free(self.lines);
        self.code = &[_]u8{};
        self.lines = &[_]Line{};
        self.constants.deinit();
    }

    /// Append a byte to the chunk along with the line number specifying where the source line came
    /// from.
    ///
    /// This may need expanding the internal array in which case the reallocation might fail.
    pub fn write(self: *Self, byte: u8, line: usize) !void {
        // handling `code`
        {
            const current_capacity = self.code.len;
            // double the arrays if appending the byte exceeds the current capacity
            if (self.code_count + 1 > current_capacity) {
                const new_capacity = grow_capacity(current_capacity);
                self.code = try self.allocator.realloc(self.code, new_capacity);
            }
            self.code[self.code_count] = byte;
            self.code_count += 1;
        }

        // handling `lines`
        {
            // check if we're still on the same line
            if (self.lines_count > 0 and self.lines[self.lines_count - 1].lineno == line) {
                return;
            }

            // we have started a new line so append a new `Line`
            const current_capacity = self.lines.len;
            if (self.lines_count + 1 > current_capacity) {
                const new_capacity = grow_capacity(current_capacity);
                self.lines = try self.allocator.realloc(self.lines, new_capacity);
            }
            self.lines[self.lines_count] = Line{
                .lineno = line,
                .offset = self.code_count - 1,
            };
            self.lines_count += 1;
        }
    }

    /// Add a new constant to the chunk.
    ///
    /// Returns the index where the constant was appended so that it can be located later easily.
    pub fn add_constant(self: *Self, value: Value, line: usize) !usize {
        const idx = self.constants.items.len;
        self.constants.append(value) catch @panic("OOM");
        switch (value) {
            .boolean => {
                try self.write(@intFromEnum(Opcode.Constant), line);
                // the operand is just a byte
                const operand: u8 = @truncate(idx);
                try self.write(operand, line);
            },
            .number, .object => {
                // if number of constants are more than 0xff, only then use CONSTANT_LONG
                if (idx <= 0xff) {
                    try self.write(@intFromEnum(Opcode.Constant), line);
                    // the operand is just a byte
                    const operand: u8 = @truncate(idx);
                    try self.write(operand, line);
                } else {
                    try self.write(@intFromEnum(Opcode.Constant_Long), line);
                    // operand is a supposed to be a 24-bit number.
                    const truncated: u24 = @truncate(idx);
                    // We perform a byte-swap if we're on a little-endian machine
                    const operand = if (comptime builtin.cpu.arch.endian() == .little)
                        @byteSwap(truncated)
                    else
                        @compileError("compiling for a Big-Endian architecture isn't supported");
                    const operand_bytes: *const [3]u8 = @ptrCast(&operand);
                    // assuming we're on a little-endian machine, the bytes will be in reverse order
                    for (operand_bytes) |byte| {
                        try self.write(byte, line);
                    }
                }
            },
        }
        return idx;
    }

    /// Disassembles the chunk as a list of instructions
    pub fn disassemble(self: *Self, writer: VMWriter, name: []const u8) !void {
        try writer.writeAll("disassembling instructions\n");
        try writer.print("=====[ {s} ]=====\n", .{name});

        var offset: usize = 0;
        while (offset < self.code_count) {
            offset = try self.disassemble_at(writer, offset);
        }
    }

    pub fn disassemble_at(self: *Self, writer: anytype, offset: usize) !usize {
        try writer.print("{x:0>4} ", .{offset});

        const current_lineno = self.get_line(offset);
        if ((offset > 0) and
            // if an instruction comes from the same source line,
            // we just display a `|` instead of the line number
            (current_lineno == self.get_line(offset - 1)))
        {
            try writer.print("{u: >5} ", .{'|'});
        } else {
            try writer.print("{d:0>5} ", .{current_lineno});
        }

        const byte = self.code[offset];
        switch (byte) {
            0...Opcode.total_variants - 1 => {
                // Safety: enumFromInt should succeed since we've checked that the value of `byte`
                // falls within the range represented by `Opcode`
                return self.disassemble_instruction(writer, @enumFromInt(byte), offset);
            },
            else => {
                try writer.print("{x:0>2} (Unknown)\n", .{byte});
                return offset + 1;
            },
        }

        return offset + 1;
    }

    pub fn disassemble_instruction(self: *Self, writer: anytype, opcode: Opcode, offset: usize) !usize {
        switch (opcode) {
            .Return,
            .Negate,
            .Not,
            .Add,
            .Subtract,
            .Multiply,
            .Divide,
            .Nil,
            .True,
            .False,
            .Equal,
            .Greater,
            .Less,
            => {
                try writer.print("{s}\n", .{opcode});
                return offset + 1;
            },
            .Constant => {
                // the operand (constant's index) should be immediately after the opcode
                const idx: usize = @as(u8, self.code[offset + 1]);
                try writer.print("{s:<8} {X:0>2} '{s}'\n", .{ opcode, idx, self.constants.items[idx] });
                return offset + 2;
            },
            .Constant_Long => {
                const bytes = [_]u8{ self.code[offset + 1], self.code[offset + 2], self.code[offset + 3] };
                // get the 24-bit operand
                const operand = @as(u24, @bitCast(bytes));
                // handle endianness
                const idx = if (comptime builtin.cpu.arch.endian() == .little) @byteSwap(operand) else operand;
                try writer.print("{s:<8} {X:0>6} '{s}'\n", .{ opcode, idx, self.constants.items[idx] });
                return offset + 4;
            },
        }
    }

    /// find the line number corresponding to the given instruction offset
    pub fn get_line(self: *Self, instruction_offset: usize) usize {
        // we need to handle the following cases:
        // 1. If we have a line with an offset that matches the given offset exactly, then
        //    the usual linear search logic should find our line.
        // 2. If the offset of the next line is greater than the given offset, then
        //    the offset falls within the range of the current line
        // 3. If we've reached to the last line, then the given offset should be in the range of the
        //    last line
        var i: usize = 0;
        while (i < self.lines_count) : (i += 1) {
            const line = self.lines[i];

            if (
            // case 1
            (line.offset == instruction_offset) or
                // case 2
                ((i + 1 < self.lines_count) and (instruction_offset < self.lines[i + 1].offset)) or
                // case 3
                (i + 1 == self.lines_count))
            {
                return line.lineno;
            }
        }

        unreachable;
    }
    fn grow_capacity(old: usize) usize {
        return if (old < 16) 16 else 2 * old;
    }
};

// ========================= TESTS =================================
test "print byte code" {
    if (comptime build_options.printBytecode) {
        const allocator = std.testing.allocator;

        var buffer = std.ArrayList(u8).init(allocator);
        defer buffer.deinit();
        const writer = buffer.writer();

        var chunk = Chunk.init(allocator);
        defer chunk.deinit();

        {
            const value: Value = .{
                .short = 10.12,
            };
            _ = try chunk.add_constant(value, 123);
        }

        {
            try chunk.write(@intFromEnum(Opcode.Return), 124);
        }

        {
            const value: Value = .{
                .long = 10.12,
            };
            _ = try chunk.add_constant(value, 125);
        }

        {
            try chunk.write(@intFromEnum(Opcode.Return), 125);
        }

        try chunk.disassemble(writer, "test chunk");

        const expected_output =
            \\=====[ test chunk ]=====
            \\0000 00123 CONSTANT 00 '1.012e1'
            \\0002 00124 RETURN
            \\0003 00125 CONSTANT 01 '1.012e1'
            \\0007     | RETURN
            \\
        ;

        const is_match = std.mem.eql(u8, buffer.items, expected_output);
        try std.testing.expect(is_match);
    }
}
