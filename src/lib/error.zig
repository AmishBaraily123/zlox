const std = @import("std");

/// Used to detect if error occured during compile time
pub const CompileError = error{
    InvalidExpression,
    InvalidNumber,
    InvalidCharacter,
    ChunkWrite,
};

/// Used to detect if error occured during runtime in the VM
pub const RuntimeError = error{
    InvalidInstruction,
    InvalidOperandType,
    DivideByZero,
    StackUnderflow,
    StackOverflow,
};
