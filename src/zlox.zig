//! This file will export all the symbols from various files within `lib/`.
//!
//! If another file needs to use a symbol from a different file, then it should do so from here.
//!
//! Example:
//!
//! `src/lib/value.zig` exports these types: `Value` and `ValueArray`. I need to use them from `src/main.zig`,
//! then in `src/main.zig` I will do the following:
//!
//! ```
//! const zlox = @import("zlox");
//! const Value = zlox.Value;
//! const ValueArray = zlox.ValueArray;
//! ```

const std = @import("std");

const chunk_lib = @import("lib/chunk.zig");
const compiler_lib = @import("lib/compiler.zig");
const error_lib = @import("lib/error.zig");
const object_lib = @import("lib/object.zig");
const opcode_lib = @import("lib/opcode.zig");
const scanner_lib = @import("lib/scanner.zig");
const table_lib = @import("lib/table.zig");
const value_lib = @import("lib/value.zig");
const vm_lib = @import("lib/vm.zig");
const writer_lib = @import("lib/writer.zig");

pub const build_options = @import("build_options");

pub const Chunk = chunk_lib.Chunk;
pub const Line = chunk_lib.Line;

pub const Compiler = compiler_lib.Compiler;

pub const RuntimeError = error_lib.RuntimeError;
pub const CompileError = error_lib.CompileError;

pub const Object = object_lib.Object;
pub const ObjectType = object_lib.ObjectType;
pub const String = object_lib.String;

pub const Opcode = opcode_lib.Opcode;

pub const Token = scanner_lib.Token;
pub const TokenType = scanner_lib.TokenType;
pub const Scanner = scanner_lib.Scanner;

pub const Table = table_lib.Table;
pub const Entry = table_lib.Entry;

pub const Value = value_lib.Value;
pub const ValueArray = value_lib.ValueArray;

pub const VM = vm_lib.VM;

pub const VMWriter = writer_lib.VMWriter;
pub const ExternalWriter = writer_lib.ExternalWriter;

test {
    std.testing.refAllDecls(@This());
}
