const std = @import("std");

pub fn build(b: *std.Build) void {
    const standard_target = b.standardTargetOptions(.{});
    const standard_optimize_mode = b.standardOptimizeOption(.{});

    // build options passed to the interpreter
    const build_options = b.addOptions();

    const verbose = b.option(bool, "verbose", "Verbose output");
    const print_bytecode = b.option(bool, "print-bytecode", "Print the bytecode of the source file");
    const display_stack = b.option(bool, "display-stack", "Display the stack contents");

    build_options.addOption(bool, "verbose", verbose orelse false);
    build_options.addOption(bool, "printBytecode", print_bytecode orelse false);
    build_options.addOption(bool, "displayStack", display_stack orelse false);

    const zlox = b.addModule("zlox", .{
        .root_source_file = .{ .path = "src/zlox.zig" },
        .target = standard_target,
        .optimize = standard_optimize_mode,
        .single_threaded = true,
    });
    zlox.addOptions("build_options", build_options);

    const exe = b.addExecutable(.{
        .name = "zlox",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = standard_target,
        .optimize = standard_optimize_mode,
        .single_threaded = true,
    });

    exe.root_module.addImport("zlox", zlox);

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const install_docs = b.addInstallDirectory(.{
        .source_dir = exe.getEmittedDocs(),
        .install_dir = .prefix,
        .install_subdir = "docs",
    });

    const docs_step = b.step("docs", "Copy documentation artifacts to prefix path");
    docs_step.dependOn(&install_docs.step);

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const wlox = b.addExecutable(.{
        .name = "wlox",
        .root_source_file = .{ .path = "src/wlox.zig" },
        .target = standard_target,
        .optimize = standard_optimize_mode,
    });
    wlox.entry = .disabled;
    wlox.rdynamic = true;
    wlox.export_table = true;
    wlox.root_module.addImport("zlox", zlox);

    const build_wlox = b.addInstallArtifact(wlox, .{ .dest_dir = .{ .override = .{ .custom = "../www/" } } });
    const wlox_step = b.step("wlox", "Build wlox");
    wlox_step.dependOn(&build_wlox.step);

    const test_step = b.step("test", "Run tests");

    const tests = b.addTest(.{
        .root_source_file = .{ .path = "src/zlox.zig" },
        .target = standard_target,
        .optimize = standard_optimize_mode,
    });

    tests.root_module.addOptions("build_options", build_options);
    const run_tests = b.addRunArtifact(tests);
    run_tests.skip_foreign_checks = true;

    test_step.dependOn(&run_tests.step);
}
