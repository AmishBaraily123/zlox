let wlox;

const input = document.getElementById("input");
const output = document.getElementById("output");
const clearBtn = document.getElementById("clear-btn");

WebAssembly.instantiateStreaming(fetch("wlox.wasm"), {
    env: {
        writeOut: (ptr, len) => {
            const msg = decodeStr(ptr, len);
            output.innerText += msg;
            output.scrollTop = output.scrollHeight;
        },
        writeErr: (ptr, len) => {
            const msg = decodeStr(ptr, len);
            output.innerText += msg;
            output.scrollTop = output.scrollHeight;
        }
    }
}).then(wasm_binary => {
    wlox = wasm_binary.instance.exports;
});

input.addEventListener('keydown', (event) => {
    if (event.key === "Enter" && !event.shiftKey) {
        event.preventDefault();
        executeCode();
    }
});

clearBtn.addEventListener('click', () => {
    clearOutput();
});

function executeCode() {
    const str = input.value;
    if (str === '') return;
    const [ptr, len, capacity] = encodeStr(str);
    const status = wlox.interpret(ptr, len);
    wlox.free(ptr, capacity);
    console.log(`Wlox returned: ${status}`);
    input.value = '';
    input.focus(); // Set focus back to the input field
}

function clearOutput() {
    output.innerText = '';
}

function encodeStr(str) {
    const capacity = str.length * 2 + 5;
    const ptr = wlox.alloc(capacity);
    if (ptr === 0) throw "Cannot allocate memory";
    const { written } = new TextEncoder().encodeInto(str, new Uint8Array(wlox.memory.buffer, ptr, capacity));
    return [ptr, written, capacity];
}

function decodeStr(ptr, len) {
    return new TextDecoder().decode(new Uint8Array(wlox.memory.buffer, ptr, len));
}
