- grammar up until now in Backus-Naur Form (BNF) parsed by a recursive descent pratt parser:
  ```
  <Declaration> ::= <LetDecl>
                  | <Statement>;
  <Statement> ::= <ExprStmt>
                | <PrintStmt>;
  <ExprStmt> -> <Expression>;
  <Expression> ::= <Primitive>
                 | <Object>
                 | <Expression> <BinaryOperator> <Expression>
                 | <UnaryOperator> <Expression>
                 | '(' <Expression> ')'
  <Primitive> ::= <Number> | <Boolean>
  <Object> ::= <StringObject>
  <BinaryOperator> ::= '+' | '-' | '*' | '/' | '==' | '!=' | '&&' | '||'
  <UnaryOperator> ::= '!' | '-'
  ```
  here, `LetDecl` would mean declaring global variables.
- statements are divided into two categories: declarations and expression. Declarations bind a new
  name to a value. The other kind - like control flow, functions, etc. - return values. note:
  `print` will be a function and calling a function is an expression statement.
- pratt parsing aka top-down operator precedence parsing parses expressions directly without
  creating parse trees using operator precedence to guide the process
- when handling errors, we need two things:
    - we don't want the compiler to exit after seeing the first error since we might want to check
      fix or at least know about all of the errors
    - we also don't want the compiler to take the faulty code and continue with regular execution
      since that may in turn be the source of further errors
   the compiler handles this by synchronization, which works like:
     - if error found, go into panic mode. in panic mode, compiler advances but nothing is actually
       evaluated
     - exit panic mode only when we reach a synchronization point. in our case, the synchronization
       point is statement boundaries.
     - statement boundary: `;`, `class`, `fn`, `let`, `for`, `if`, `while`, `print`, `return`.
- strings are immutable (like in Java): if you modify a string you get a newly allocated string
- all strings are interned (like in Lua): VM maintains its own collection of interned (internal) strings. duplicate
  strings have the same address. Thus pointer equality exactly matches value equality.
- global variables are lazy bound. This means:
```lox
fun showVariable() {
    print global;
}

var global = "after";
showVariable();
```
- but a local variable's declaration should always occur before it is used
